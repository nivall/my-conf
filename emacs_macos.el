;;;; my-config ---
;;;; Author: nivall
;;;; gitlab.com/nivall/my-conf

;;;; LOCAL REQUIREMENT
;; Local MacOS configuration file must contain the following two lines
;; (setq user-init-file "~/my-conf/emacs_macos.el")
;; (load user-init-file)

;;;; SYNCHRONIZED CONFIG
;; Custom theme --------------------------------------------------------------------
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["#242424" "#e5786d" "#95e454" "#cae682" "#8ac6f2" "#333366" "#ccaa8f" "#f6f3e8"])
 '(custom-enabled-themes '(deeper-blue))
 '(doc-view-continuous t)
 '(ess-R-font-lock-keywords
   '((ess-R-fl-keyword:keywords . t)
     (ess-R-fl-keyword:constants . t)
     (ess-R-fl-keyword:modifiers . t)
     (ess-R-fl-keyword:fun-defs . t)
     (ess-R-fl-keyword:assign-ops . t)
     (ess-R-fl-keyword:%op% . t)
     (ess-fl-keyword:fun-calls)
     (ess-fl-keyword:numbers . t)
     (ess-fl-keyword:operators . t)
     (ess-fl-keyword:delimiters . t)
     (ess-fl-keyword:= . t)
     (ess-R-fl-keyword:F&T . t)))
 '(ess-assign-list '("=" " = " " <- " " == " " != "))
 '(ess-eval-region-or-function-or-paragraph vis)
 '(package-selected-packages
   '(langtool languagetool ac-ispell auctex counsel ivy-bibtex org-ref markdown-mode list-packages-ext exec-path-from-shell ## magit elpy ess)))

;; Initial window size
(setq initial-frame-alist
      '(
        (width . 215) ; character
        (height . 60) ; lines
        ))

;; Cursors options
(set-cursor-color "#ffee70")
(blink-cursor-mode 0)

;; Enable automatic line break
(global-visual-line-mode t)

;; ---- Enable IVY (melpa: ivy)
(ivy-mode)

;; custom functions -------------------------------------------------------------
;; new-shell creates a new shell buffer no matter if another shell is opened
(defun new-shell ()
  (interactive)
  (let (
        (currentbuf (get-buffer-window (current-buffer)))
        (newbuf     (generate-new-buffer-name "*Shell*"))
       )
   (generate-new-buffer newbuf)
   (set-window-dedicated-p currentbuf nil)
   (set-window-buffer currentbuf newbuf)
   (vterm newbuf)
  )
)

;; set *scratch* mode to ESS -----------------------------------------------------
(setq initial-major-mode 'ess-r-mode)
(setq initial-scratch-message "## Hello Nicolas!")
(add-hook 'ess-mode-hook
          (lambda () 
            (ess-toggle-underscore nil)))  ; prevent "_" to be used as "=" 
	   
;; set terminal color to run remote tmux -----------------------------------------
(add-to-list 'term-file-aliases
    '("st-256color" . "xterm-256color"))

;; set mac keyboard options ------------------------------------------------------
(setq default-input-method "MacOSX")
(setq mac-right-command-modifier 'meta)
(setq mac-left-command-modifier 'super)
(setq mac-option-modifier 'none)
(put 'downcase-region 'disabled nil)

;; Write backups to ~/.emacs.d/backup/ -------------------------------------------
(setq backup-directory-alist '(("." . "~/.emacs.d/backup"))
      backup-by-copying      t  ; Don't de-link hard links
      version-control        t  ; Use version numbers on backups
      delete-old-versions    t  ; Automatically delete excess backups:
      kept-new-versions      20 ; how many of the newest versions to keep
      kept-old-versions      5) ; and how many of the old

;; dont show GNU emacs screen at startup ----------------------------------------
(setq inhibit-startup-screen t)

;; define shortcuts and remap keys ----------------------------------------------
(defalias 'yes-or-no-p             'y-or-n-p       )
(defalias 'list-buffers            'ibuffer        )
(global-set-key (kbd "M-o")        'other-window   )
(global-set-key (kbd "M-<left>")   'previous-buffer)
(global-set-key (kbd "M-<right>")  'next-buffer    )
(global-set-key (kbd "C-c ;")      'comment-line   )
(global-set-key (kbd "M-S"   )     'new-shell      )
(global-set-key (kbd "s-f"   )     'swiper-isearch )
(global-set-key (kbd "="     )     'ess-cycle-assign) ; ess arrow or equal signs
(global-set-key (kbd "C->"   )     #'(lambda () ; enlarge window by 5 
				      (interactive)
				      (enlarge-window-horizontally 5) ) )
(global-set-key (kbd "C-<"   )     #'(lambda () ; enlarge window by 5 
				      (interactive)
				      (shrink-window-horizontally 5) ) )
(global-set-key (kbd "M-°"   )     'backward-paragraph)
(global-set-key (kbd "M-+"   )     'forward-paragraph )
(global-set-key (kbd "C-="   )     "%>% "             )
(global-set-key (kbd "M-#"   )     "## ----------------------------------------------------------------" )
(global-set-key (kbd "C-c d" )     'doi-insert-bibtex )

;; Define default split screen to the vertical ----------------------------------
(setq split-width-threshold 0)
(setq split-height-threshold nil)

;; define default directory when search files -----------------------------------
(setq default-directory "~/")
(setq command-line-default-directory "~/")

;;;; Custom packages ------------------------------------------------------------
;; ---- MELPA 
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(add-to-list 'package-archives
             '("melpa-stable" . "https://stable.melpa.org/packages/") t)

;; ---- R
;; get ESS (download sources and unpack in ~/ESS-master
(add-to-list 'load-path "~/ESS-master/lisp")
;;(setq inferior-R-program-name "/usr/bin/R")
;;(setq-default inferior-R-program-name "/usr/local/bin/R")
(executable-find "R")
(require 'ess-r-mode)

;; ---- Enable LaTeX export to pdf (install MacTex)
(exec-path-from-shell-initialize)

;; ---- Enable org-ref
(require 'org-ref)

;; ---- Julia
(add-to-list 'load-path "/usr/local/bin/julia")
(require 'julia-repl)
(add-hook 'julia-mode-hook 'julia-repl-mode) ;; always use minor mode
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

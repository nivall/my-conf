;;;; my-config --- Ubuntu
;;;; Author: nivall
;;;; gitlab.com/nivall/my-conf

;;;; LOCAL REQUIREMENT
;; Local Ubuntu configuration file must contain the following two lines
;; (setq user-init-file "~/my-conf/ubuntu.el")
;; (load user-init-file)

;;;; SYNCHRONIZED CONFIG
;; Custom theme --------------------------------------------------------------------
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["#242424" "#e5786d" "#95e454" "#cae682" "#8ac6f2" "#333366" "#ccaa8f" "#f6f3e8"])
 '(custom-enabled-themes '(deeper-blue))
 '(doc-view-continuous t)
 '(ess-R-font-lock-keywords
   '((ess-R-fl-keyword:keywords . t)
     (ess-R-fl-keyword:constants . t)
     (ess-R-fl-keyword:modifiers . t)
     (ess-R-fl-keyword:fun-defs . t)
     (ess-R-fl-keyword:assign-ops . t)
     (ess-R-fl-keyword:%op% . t)
     (ess-fl-keyword:fun-calls)
     (ess-fl-keyword:numbers . t)
     (ess-fl-keyword:operators . t)
     (ess-fl-keyword:delimiters . t)
     (ess-fl-keyword:= . t)
     (ess-R-fl-keyword:F&T . t)))
 '(ess-assign-list '("=" " = " " <- " " == " " != "))
 '(ess-eval-region-or-function-or-paragraph vis)
 '(warning-suppress-types '((comp) (comp))))

;; Initial window size
(setq initial-frame-alist
      '(
        (width . 215) ; character
        (height . 60) ; lines
        ))

;; Cursors options
(set-cursor-color "#ffee70")
(blink-cursor-mode 0)

;; Enable automatic line break
(global-visual-line-mode t)

;; set *scratch* mode to ESS -----------------------------------------------------
(add-hook 'ess-mode-hook
          (lambda () 
            (ess-toggle-underscore nil)))  ; prevent "_" to be used as "=" 

;; Write backups to ~/.emacs.d/backup/ -------------------------------------------
(setq backup-directory-alist '(("." . "~/.emacs.d/backup"))
      backup-by-copying      t  ; Don't de-link hard links
      version-control        t  ; Use version numbers on backups
      delete-old-versions    t  ; Automatically delete excess backups:
      kept-new-versions      20 ; how many of the newest versions to keep
      kept-old-versions      5) ; and how many of the old

;; dont show GNU emacs screen at startup ----------------------------------------
(setq inhibit-startup-screen t)

;; define shortcuts and remap keys ----------------------------------------------
(defalias 'yes-or-no-p             'y-or-n-p       )
(defalias 'list-buffers            'ibuffer        )
(global-set-key (kbd "M-o")        'other-window   )
(global-set-key (kbd "M-<left>")   'previous-buffer)
(global-set-key (kbd "M-<right>")  'next-buffer    )
(global-set-key (kbd "C-c ;")      'comment-line   )
(global-set-key (kbd "\C-f"   )     'swiper-isearch )
(global-set-key (kbd "="     )     'ess-cycle-assign) ; ess arrow or equal signs
(global-set-key (kbd "C->"   )     #'(lambda () ; enlarge window by 5 
				      (interactive)
				      (enlarge-window-horizontally 5) ) )
(global-set-key (kbd "C-<"   )     #'(lambda () ; enlarge window by 5 
				      (interactive)
				      (shrink-window-horizontally 5) ) )
(global-set-key (kbd "M-°"   )     'backward-paragraph)
(global-set-key (kbd "M-+"   )     'forward-paragraph )
(global-set-key (kbd "C-="   )     "%>% "             )
(global-set-key (kbd "M-#"   )     "## ----------------------------------------------------------------" )

;; Define default split screen to the vertical ----------------------------------
(setq split-width-threshold 0)
(setq split-height-threshold nil)

;; define default directory when search files -----------------------------------
(setq default-directory "~/")
(setq command-line-default-directory "~/")

;; ---- Enable IVY (melpa: ivy)
(ivy-mode)


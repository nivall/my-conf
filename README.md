# My configuration files
Clone this repository to root and get started.

## Emacs config
### For MacOS environments
Add the following lines to the file `.emacs`
```
(setq user-init-file "~/my-conf/emacs_macos.el")
(load user-init-file)
```

### For Linux environments
WIP
